/*******************************************************
 * 
 * 文件名(File Name)：             CreateConfigDataIniFile
 *
 * 作者(Author)：                  http://www.youkexueyuan.com
 *								  XiaoHong 
 *                                Yangzj
 *
 * 创建时间(CreateTime):           2016/03/08 12:18:44
 *
 *******************************************************/

using UnityEngine;
using System.Collections;
using UnityEditor;
using System.IO;
using System.Collections.Generic;
using MyFrameWork;

public class CreateConfigDataIniFile : MonoBehaviour 
{
	[MenuItem("Tools/CreateConfigDataIniFile")]
	public static void Createini()
	{

		List<string> list = new List<string>();
		string pathRes = Application.dataPath +"/Resources/";
		string pathIni = pathRes + "/configData.txt";
		if (File.Exists(pathIni))
		{
			File.Delete(pathIni);
		}

		CreateResInfo(ConfigDataMgr.BinaryDataPath, ref list);
		File.WriteAllLines(pathRes +"/configData.txt",list.ToArray());
		DebugUtil.Debug("生成完毕 ");
		AssetDatabase.Refresh();
	}

	public static void CreateResInfo(string path,ref List<string> list)
	{
		DirectoryInfo dir = new DirectoryInfo(path);
		if (!dir.Exists)
		{
			return;
		}
		FileInfo[] files = dir.GetFiles();
		for (int i = 0; i < files.Length;i++ )
		{

			FileInfo info = files[i];

			if(info.Name.Equals(".DS_Store") || info.Name.EndsWith(".txt"))
				continue;
			
			if (!(info.Name.IndexOf(".meta",0) > 0))
			{
				string fileName = Path.GetFileNameWithoutExtension(info.Name);
				DebugUtil.Info("fileName =" + fileName);

				if (!list.Contains(fileName))
				{
					list.Add(fileName);
				}
				else
				{
					DebugUtil.Error("存在相同的资源名称 名称为：" + info.Name);
				}
			}
		}
		DirectoryInfo[] dirs = dir.GetDirectories();
		if (dirs.Length > 0)
		{
			for (int i = 0; i < dirs.Length;i++ )
			{
				string tempPath = Path.Combine(path, dirs[i].Name);
				CreateResInfo(tempPath, ref list);
			}
		}
	}
}
