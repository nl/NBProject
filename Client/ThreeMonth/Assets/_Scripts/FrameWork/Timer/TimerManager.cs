/*******************************************************
 * 
 * 文件名(File Name)：             SpriteTitleChange.cs
 *
 * 作者(Author)：                  http://www.youkexueyuan.com
 *								  XiaoHong 
 *                                Yangzj
 *
 * 创建时间(CreateTime):           2016/03/02 13:48:41
 *
 *******************************************************/

using UnityEngine;
using System.Collections;

namespace MyFrameWork
{
	public class TimerManager : Singleton<TimerManager>
	{
		public static TimerBehaviour GetTimer(GameObject target)
		{
			return target.GetOrAddComponent<TimerBehaviour>();
		}

		public void Update()
		{
			FrameTimerHeap.Tick();
		}

		protected override void OnReleaseValue()
		{
			FrameTimerHeap.ReleaseVal();
		}

		protected override void OnAppQuit()
		{
			OnReleaseValue();
		}
	}
}