using System;
namespace MyFrameWork
{
    public interface IDataReader
    {
        string path { get; set; }
        void Init(String a_Path);
        Row[] ReadData();
        Schema ReadSchema();
    }
}
