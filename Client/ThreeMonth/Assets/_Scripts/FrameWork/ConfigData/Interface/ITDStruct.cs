using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyFrameWork
{
	public interface ITDStruct
	{
        void Init(Row row);
	}

	public interface ITDStruct<TKey>
	{
		TKey KeyItem{get;}
		void Init(Row row);
	}
}
