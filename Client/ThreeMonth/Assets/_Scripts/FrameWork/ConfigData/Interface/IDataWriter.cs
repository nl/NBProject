using System;
namespace MyFrameWork
{
    public interface IDataWriter
    {
        string path { get; set; }
        void Init(String a_Path);
        bool Write(Schema schema,Row[] rows);
    }
}
