using UnityEngine;
using MyFrameWork;

public class TDMonoTunner : MonoBehaviour
{
    Row m_Row = null;

    public Row Row
    {
        get { return m_Row; }
        set { m_Row = value; }
    }
    string m_TableName = null;

    public string TableName
    {
        get { return m_TableName; }
        set { m_TableName = value; }
    }
    public void SetDataSource(string tableName, Row line)
    {
        m_TableName = tableName;
        m_Row = line;
    }
}
