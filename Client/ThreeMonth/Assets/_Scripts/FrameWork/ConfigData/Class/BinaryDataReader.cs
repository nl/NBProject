using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using UnityEngine;

namespace MyFrameWork
{
    public class BinaryDataReader : IDataReader
    {
        string m_Path;
        string m_FileName;
        //string m_suffix;
        int m_recordsNumber = 0;
        int m_Identity = 82305;

        BinaryReader m_Reader = null;
        private static FileStream m_stream = null;
        List<Row> mRowList = new List<Row>();
        public string path
        {
            get { return m_Path; }
            set { m_Path = value; }
        }

        Schema m_Schema = null;

        public BinaryDataReader(String a_path)
        {
            Init(a_path);
        }
        public void Init(String a_path)
        {
            path = a_path;
            string[] sArr1 = path.Split(new char[] { '/' });
            string str1 = sArr1[sArr1.Length - 1];
            string[] sArr2 = str1.Split(new char[] { '.' });
            m_FileName = sArr2[0];
            //m_suffix = sArr2[1];
            bool readerInited = InitReader();
            if (!readerInited)
            {
                return;
            }
            LoadSchema();
            LoadData();
            closeFile();
        }

        void closeFile()
        {
            if (m_stream != null)
            {
                m_stream.Close();
                
            }

            if (m_Reader != null)
            {
                m_Reader.Close();
            }
        }

        public bool InitReader()
        {
            if (string.IsNullOrEmpty(path))
            {
                return false;
            }
                if (!File.Exists(path))
                    return false;
                m_stream = new FileStream(path, FileMode.Open, FileAccess.Read);
                m_Reader = new BinaryReader(m_stream);
            
            return true;
        }

        void LoadSchema()
        {
            if (m_Schema == null)
            {
                m_Schema = new Schema();
                m_Schema.ClassName = m_FileName;

                int identity = m_Reader.ReadInt32();
                if (m_Identity != identity)
                {
                    return;
                }
                //read record number
                m_recordsNumber = m_Reader.ReadInt32();

                //read column number
                int columnNumber = m_Reader.ReadInt32();

                //create field define and fill field name.
                for (int i = 0; i < columnNumber; ++i)
                {
                    FieldDefine define = new FieldDefine();
                    define.FieldName = m_Reader.ReadString();
                    define.Index = i;
                    m_Schema.AddDefine(define);
                }

                //read field type.
                for (int i = 0; i < columnNumber; ++i)
                {
                    FieldDefine define = m_Schema.GetDefine(i);
                    define.FieldType = (FIELD_TYPE)m_Reader.ReadInt32();
                }
     
            }
            else
            {
				DebugUtil.Info("Schema is null!!!");
            }
       
        }

        //load Data To Reader
         List<object> tempVar = new List<object>();
        void LoadData()
        {
            if (m_Schema == null)
            {
                return;
            }
            for (int i = 0; i < m_recordsNumber; ++i)
            {
                Row tempRow = new Row(m_Schema);
                for (int j = 0; j < m_Schema.Count; ++j)
                {
                    FieldDefine tempDefine = m_Schema.GetDefine(j);
                    Field tempField = null;
                    switch (tempDefine.FieldType)
                    {
                        case FIELD_TYPE.T_ENUM:
                        case FIELD_TYPE.T_INT:
                            int tempInt = m_Reader.ReadInt32();
                            tempField = new Field(tempInt);
                            break;
                        case FIELD_TYPE.T_FLOAT:
                            float tempFloat = m_Reader.ReadSingle();
                            tempField = new Field(tempFloat);
                            break;
                        case FIELD_TYPE.T_STRING:
                            string tempString = m_Reader.ReadString();
                          
                            tempField = new Field(tempString);
                            break;
                        case FIELD_TYPE.T_ARRINT:
                            {
                                int arrlen = this.m_Reader.ReadInt16();
                                List<object> lint = new List<object>();
                                for (int li = 0; li < arrlen; ++li)
                                {
                                    lint.Add(this.m_Reader.ReadInt32());
                                }
                                tempField = new Field(lint.ToArray());
                                break;
                            }
                        case FIELD_TYPE.T_ARRFLOAT:
                            {
                                int arrlen = this.m_Reader.ReadInt16();
                                List<object> lfloat = new List<object>();
                                for (int fi = 0; fi < arrlen; ++fi)
                                {
                                    lfloat.Add(this.m_Reader.ReadSingle());
                                }
                                tempField = new Field(lfloat.ToArray());
                                break;
                            }
                        case FIELD_TYPE.T_UNITE:
                            int temp = m_Reader.ReadInt32();
                            tempField = new Field(temp);
                            break;
                        default:
                            tempField = new Field(0);
							DebugUtil.Info("Undefined Field Type");
                            break;
                    }
                    tempRow.m_Fields.Add(tempField);
                }
                mRowList.Add(tempRow);
            }
        }
        void ParseString(string str)
        {
            int i, val_i = 0;
            double val_d = 0.0;
            string val_s = "";
            string[] strs = str.Split(',');

            for (i = 0; i < strs.Length; i++)
            {
              
                //if (IsNested(strs[i]))
                //{
                  
                //    char[] nest = new char[strs[i].Length - 7];
                //    for (j = 6; j < strs[i].Length - 1; j++)
                //        nest[j - 6] = strs[i][j];

                //    ParseString(new string(nest));


                //}
                //else
                //{
                    try
                    {
                        val_i = Convert.ToInt32(strs[i]);
                        tempVar.Add(val_i);
                       
                    }
                    catch
                    {
                        try
                        {
                            val_d = Convert.ToDouble(strs[i]);
                            tempVar.Add((float)val_d);
                        }
                        catch
                        {
                            val_s = strs[i];
                            tempVar.Add(val_s);
                        }
                    }

               // }
            }


        }
        //bool IsNested(string str)
        //{

        //    if (str == null || str.Length < 7) //unite()
        //        return false;
        //    if (str[0] == 'u' && str[1] == 'n' && str[2] == 'i' && str[3] == 't' && str[4] == 'e' && str[5] == '(' && str[str.Length - 1] == ')')
        //        return true;

        //    return false;
        //}
        public Schema ReadSchema()
        {
            return m_Schema;
        }

        public Row[] ReadData()
        {

            if (mRowList.Count > 0)
            {
                return mRowList.ToArray();
            }
            else
            {
                return null;
            }
        }
    }
}
