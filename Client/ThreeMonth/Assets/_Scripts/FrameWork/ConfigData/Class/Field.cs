using System.Collections.Generic;
using UnityEngine;

namespace MyFrameWork
{
    public class Field
    {
        public Field(object value)
        {
            m_Value = value;
        }
        public object m_Value;
        public object[] m_ValueList;
        public Field(object[] value)
        {
            m_ValueList = new object[value.Length];
           
            for (int i = 0; i < value.Length; i++)
            {
                m_ValueList[i] = value[i];
            }

        }
        public int GetInt()
        {
            return (int)m_Value;
        }


        public float GetFloat()
        {
            return (float)m_Value;
        }
        public string GetString()
        {
            return m_Value.ToString();
        }
		
        public float[] GetFloatArr()
        {
            if(null==m_ValueList)
            {
                return null;
            }

            List<float> arr = new List<float>();
            for (int i = 0; i < m_ValueList.Length;++i )
            {
                arr.Add((float)m_ValueList[i]);
            }

            return arr.ToArray();
        }

        public int[] GetIntArr()
        {
            if (null == m_ValueList)
            {
                return null;
            }

            List<int> arr = new List<int>();
            for (int i = 0; i < m_ValueList.Length;++i )
            {
                arr.Add((int)m_ValueList[i]);
            }

            return arr.ToArray();
        }


//        public string GetUnite()
//        {
//            int index = 0;
//
//            index = (int)m_Value;
//
//            //string str = GameStrManager.GetStr(strStaticData.mIdxAchievement_Name_0 + (uint)index);
//            string str = GameStrManager.GetUniteStr((uint)index);
//            return str;
//        }
    }
}