/*******************************************************
 * 
 *******************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TDStruct;

namespace MyFrameWork
{
	public interface IReloadable 
	{

		void LoadData();

		void ReloadData();

	}

	public class BaseDataMgr<TMgr,TItem,Tkey> :IReloadable 
		where TMgr : new() 
		where TItem : class,ITDStruct<Tkey>,new()
	{
		private Dictionary<Tkey, TItem> _dicItem = new Dictionary<Tkey, TItem>();
		public Dictionary<Tkey,TItem> DicItem
		{
			get
			{
				return _dicItem;
			}
		}

		static TMgr sIntance;
		public static TMgr Instance
		{
			get
			{
				if (sIntance == null)
				{
					sIntance = new TMgr();
				}
				return sIntance;
			}
		}

		public void LoadData()
		{
			TItem[] table = TDRoot.Instance.getTable(typeof(TItem).Name).GetStructListNew<TItem,Tkey>();
			for (int nCount = 0; nCount < table.Length; ++nCount)
			{
				_dicItem.Add(table[nCount].KeyItem, table[nCount]);
			}
		}
		public void ReloadData()
		{
			LoadData();
		}

		public TItem GetItemByID(Tkey nItemId)
		{
			if (_dicItem.ContainsKey(nItemId))
				return _dicItem[nItemId];
			else
				return null;
		}
	}
}
