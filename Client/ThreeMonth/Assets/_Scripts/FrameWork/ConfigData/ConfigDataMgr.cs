/*******************************************************
 * 
 * 文件名(File Name)：             SpriteTitleChange.cs
 *
 * 作者(Author)：                  http://www.youkexueyuan.com
 *								  XiaoHong 
 *                                Yangzj
 *
 * 创建时间(CreateTime):           2016/03/08 10:35:02
 *
 *******************************************************/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace MyFrameWork
{
	public class ConfigDataMgr : Singleton<ConfigDataMgr> 
	{
		public static string BinaryDataPath = Application.dataPath + "/ResourcesRaw/ConfigDataTable/";

		private bool mLoaded = false;
		private List<IReloadable> _reloadableList = null;

		public override void Init()
		{
			_reloadableList = new List<IReloadable>();
		}

		public void AddDataManager(IReloadable reloadable)
		{
			_reloadableList.Add(reloadable);
		}

		private void LoadAllData()
		{
			if (mLoaded == false)
			{
				for (int i = 0; i < _reloadableList.Count;++i )
				{
					_reloadableList[i].LoadData();
				}

				mLoaded = true;
			}
		}

		protected override void OnReleaseValue()
		{
			_reloadableList.Clear();
		}

		protected override void OnAppQuit()
		{
			_reloadableList = null;
		}

		/// <summary>
		/// 加载所有配置表数据
		/// </summary>
		public void LoadConfigData()
		{
			GetListFile();

//			return;
			for(int i = 0;i < _listFile.Count;i++)
			{
				TDRoot.Instance.Open(string.Concat(BinaryDataPath,_listFile[i],".bytes"));
			}
//
//			TDRoot.Instance.Open(string.Concat(BinaryDataPath,"Card",".bytes"));

			AddDataManager(CardDataMgr.Instance);
			AddDataManager(TestTableMgr.Instance);

			LoadAllData();
		}

		#region 获取资源对应的全路径
		private List<string> _listFile = null;

		private void GetListFile()
		{
			_listFile = new List<string>();

			UnityEngine.TextAsset tex = Resources.Load<TextAsset>("configData");
			StringReader sr = new StringReader(tex.text);
			string fileName = sr.ReadLine();
			while (fileName != null)
			{
				_listFile.Add(fileName);
				fileName = sr.ReadLine();
			}

			sr.Close();
			sr.Dispose();
		}

		#endregion
	}
}
