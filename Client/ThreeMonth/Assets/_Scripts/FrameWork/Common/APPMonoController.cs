/*******************************************************
 * 
 * 文件名(File Name)：             CoroutineController
 *
 * 作者(Author)：                  Yangzj
 *
 * 创建时间(CreateTime):           2016/02/25 17:12:38
 *
 *******************************************************/

using UnityEngine;
using System.Collections;

namespace MyFrameWork
{
	public class APPMonoController : DDOLSingleton<APPMonoController>
	{
		public void StartUpdate()
		{	
		}

		void Update()
		{
			ResourceMgr.Instance.Update();
			UIMgr.Instance.Update();
			TimerManager.Instance.Update();
		}

		/// <summary>
		/// 返回登陆界面，清除变量
		/// </summary>
		public override void ReleaseValue()
		{
			UIMgr.Instance.ReleaseValue();
			ResourceMgr.Instance.ReleaseValue();
			EventDispatcher.ReleaseValue();
			TimerManager.Instance.ReleaseValue();
		}

		void OnApplicationQuit()
		{
			UIMgr.Instance.OnApplicationQuit();
			ResourceMgr.Instance.OnApplicationQuit();
			EventDispatcher.OnApplicationQuit();
			TimerManager.Instance.OnApplicationQuit();
		}
	}
}
