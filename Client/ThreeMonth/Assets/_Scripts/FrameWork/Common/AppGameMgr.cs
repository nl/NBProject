/*******************************************************
 * 
 * 文件名(File Name)：             AppGameMgr
 *
 * 作者(Author)：                  Yangzj
 *
 * 创建时间(CreateTime):           2016/02/29 18:43:30
 *
 *******************************************************/

using UnityEngine;
using System.Collections;

namespace MyFrameWork
{
	public class AppGameMgr : MonoBehaviour 
	{
		// Use this for initialization
		void Awake () 
		{
			Application.targetFrameRate = 60;
			QualitySettings.vSyncCount = 0;

			//启动manager
			APPMonoController.Instance.StartUpdate();

			//配置文件
			ConfigDataMgr.Instance.LoadConfigData();

			//加载module
			ModuleMgr.Instance.RegisterAllModules();

		}
	}
}
