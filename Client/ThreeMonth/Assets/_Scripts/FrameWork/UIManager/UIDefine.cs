/*******************************************************
 * 
 * 文件名(File Name)：             UIDefine
 *
 * 作者(Author)：                  http://www.youkexueyuan.com
 *								  XiaoHong 
 *                                Yangzj
 *
 * 创建时间(CreateTime):           2016/02/29 12:51:24
 *
 *******************************************************/

using UnityEngine;
using System.Collections;

namespace MyFrameWork
{
	/// <summary>
	/// 状态变化
	/// </summary>
	public delegate void StateChangedEvent (object sender,E_ObjectState newState,E_ObjectState oldState);

	public enum E_UIType
	{
		/// <summary>
		/// The none.
		/// </summary>
		None = -1,
		//test ui
		PanelTestMain,
		PanelTestLogin,
		PanelTestMail,
		PanelTestTopBar,

		//formal ui
	}

	public enum E_ObjectState
	{
		/// <summary>
		/// The none.
		/// </summary>
		None,
		/// <summary>
		/// The initial.
		/// </summary>
		Initial,
		/// <summary>
		/// The loading.
		/// </summary>
		Loading,
		/// <summary>
		/// The ready.
		/// </summary>
		Ready,
		/// <summary>
		/// The disabled.
		/// </summary>
		Disabled,
		/// <summary>
		/// The closing.
		/// </summary>
		Closing
	}

	public enum E_UIShowAnimStyle
	{
		Normal,
		/// <summary>
		/// 中间由小变大
		/// </summary>
		CenterScaleBigNormal,
		/// <summary>
		/// 由上往下
		/// </summary>
		TopToSlide,
		/// <summary>
		/// 由下往上
		/// </summary>
		DownToSlide,
		/// <summary>
		/// 由左往中
		/// </summary>
		LeftToSlide,
		/// <summary>
		/// 由右往中
		/// </summary>
		RightToSlde,
	}

	public enum E_UIMaskStyle
	{
		/// <summary>
		/// 无背景
		/// </summary>
		None,
		/// <summary>
		/// 半透明背景
		/// </summary>
		BlackAlpha,
		/// <summary>
		/// 无背景，但有boxclider关闭组件
		/// </summary>
		Alpha,
	}

}
