/*******************************************************
 * 
 * 文件名(File Name)：             CardDataMgr.cs
 *
 * 作者(Author)：                  http://www.youkexueyuan.com
 *								  XiaoHong 
 *                                Yangzj
 *
 * 创建时间(CreateTime):           2016/03/07 19:15:15
 *
 *******************************************************/

using UnityEngine;
using System.Collections;
using MyFrameWork;
using TDStruct;
public class CardDataMgr : BaseDataMgr<CardDataMgr,Card,int>
{
	public string GetCardName(int itemId)
	{
		Card card = DicItem[itemId];
		if(card != null) 
			return "CardName:" + card.Desc;
		else 
			return string.Empty;
	}
}
