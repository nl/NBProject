/*******************************************************
 * 
 * 文件名(File Name)：             UITopBar
 *
 * 作者(Author)：                  http://www.youkexueyuan.com
 *								  XiaoHong 
 *                                Yangzj
 *
 * 创建时间(CreateTime):           2016/03/10 15:03:05
 *
 *******************************************************/

using UnityEngine;
using System.Collections;
using MyFrameWork;

public class UITopBar : BaseUI
{

	public override E_UIType GetUIType()
	{
		return E_UIType.PanelTestTopBar;
	}

	private UILabel _labelGold;
	private UIButton _btnAddGold;

	private UILabel _labelEnergy;
	private UIButton _btnAddEnergy;

	private ModuleTopBar _moduleTopBar;
	private ModuleTopBar ModuleTopBar
	{
		get
		{
			if(_moduleTopBar == null)
				_moduleTopBar = ModuleMgr.Instance.Get<ModuleTopBar>();

			return _moduleTopBar;
		}
	}

	protected override void OnInit()
	{
		base.OnInit();

		_labelGold = CachedTransform.Find("Gold/Label").GetComponent<UILabel>();
		_btnAddGold = CachedTransform.Find("Gold/BtnAdd").GetComponent<UIButton>();
	
		_labelEnergy = CachedTransform.Find("Energy/Label").GetComponent<UILabel>();
		_btnAddEnergy = CachedTransform.Find("Energy/BtnAdd").GetComponent<UIButton>();
	}

	protected override void OnBtnClick(GameObject go)
	{
		if(go == _btnAddGold.gameObject)
		{
			ModuleTopBar.AddGold(10);
		}
		else if(go == _btnAddEnergy.gameObject)
		{
			ModuleTopBar.AddEnergy(1);
		}
	}

	void OnEnable()
	{
		EventDispatcher.AddListener<ModuleTopBar.E_TopBarItemType,int>("UpdateTopBar",OnUpdateTopBarInfo);
	}

	void OnDisable()
	{
		EventDispatcher.RemoveListener<ModuleTopBar.E_TopBarItemType,int>("UpdateTopBar",OnUpdateTopBarInfo);
	}

	private void OnUpdateTopBarInfo(ModuleTopBar.E_TopBarItemType type,int val)
	{
		switch(type)
		{
			case ModuleTopBar.E_TopBarItemType.Gold:
				_labelGold.text = val.ToString();
				break;
			case ModuleTopBar.E_TopBarItemType.Energy:
				_labelEnergy.text = val.ToString();
				break;
			default:
				DebugUtil.Error("OnUpdateTopBarInfo - unrealized topBarType: " + type.ToString());
				break;
		}
	}
}
