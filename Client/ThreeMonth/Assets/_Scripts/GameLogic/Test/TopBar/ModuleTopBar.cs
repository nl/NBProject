/*******************************************************
 * 
 * 文件名(File Name)：             ModuleTopBar
 *
 * 作者(Author)：                  http://www.youkexueyuan.com
 *								  XiaoHong 
 *                                Yangzj
 *
 * 创建时间(CreateTime):           2016/03/10 15:02:52
 *
 *******************************************************/

using UnityEngine;
using System.Collections;
using MyFrameWork;

public class ModuleTopBar : BaseModule
{
	public enum E_TopBarItemType
	{
		Gold,
		Energy,
	}
	public int Gold {get;private set;}
	public int Energy{get;private set;}

	public ModuleTopBar()
	{
		AutoRegister = true;
	}

	public void AddGold(int gold)
	{
		Gold += gold;
		EventDispatcher.TriggerEvent<E_TopBarItemType,int>("UpdateTopBar",E_TopBarItemType.Gold,Gold);
	}

	public void AddEnergy(int energy)
	{
		Energy += energy;
		EventDispatcher.TriggerEvent<E_TopBarItemType,int>("UpdateTopBar",E_TopBarItemType.Energy,Energy);
	}
}
