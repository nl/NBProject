/*******************************************************
 * 
 * 文件名(File Name)：             UIMail
 *
 * 作者(Author)：                  Yangzj
 *
 * 创建时间(CreateTime):           2016/02/29 18:42:03
 *
 *******************************************************/

using UnityEngine;
using System.Collections;
using MyFrameWork;

public class UIMail : BaseUI 
{
	public override E_UIType GetUIType()
	{
		return E_UIType.PanelTestMail;
	}

	protected override void OnInit()
	{
		base.OnInit();
		_animationStyle = E_UIShowAnimStyle.CenterScaleBigNormal;
	}

	protected override void OnBtnClick(GameObject go)
	{
		switch(go.name)
		{
			case "BtnClose":
				UIMgr.Instance.ShowUIAndCloseOthers(E_UIType.PanelTestMain,typeof(UIMain),null);
				break;
		}
	}
}
