/*******************************************************
 * 
 * 文件名(File Name)：             UILogin
 *
 * 作者(Author)：                  Yangzj
 *
 * 创建时间(CreateTime):           2016/02/29 18:41:06
 *
 *******************************************************/

using UnityEngine;
using System.Collections;
using MyFrameWork;

public class UILogin : BaseUI 
{
	public override E_UIType GetUIType()
	{
		return E_UIType.PanelTestLogin;
	}

	protected override void OnInit()
	{
		base.OnInit();
		_animationStyle = E_UIShowAnimStyle.LeftToSlide;
	}

	protected override void OnBtnClick(GameObject go)
	{
		switch(go.name)
		{
			case "BtnClose":
				UIMgr.Instance.ShowUIAndCloseOthers(E_UIType.PanelTestMain,typeof(UIMain),null);
				break;
		}
	}
}
