/*******************************************************
 * 
 * 文件名(File Name)：             UIMain
 *
 * 作者(Author)：                  Yangzj
 *
 * 创建时间(CreateTime):           2016/02/29 18:38:20
 *
 *******************************************************/

using UnityEngine;
using System.Collections;
using MyFrameWork;

public class UIMain : BaseUI 
{
	public override E_UIType GetUIType()
	{
		return E_UIType.PanelTestMain;
	}

	protected override void OnBtnClick(GameObject go)
	{
		switch(go.name)
		{
			case "BtnLogin":
				UIMgr.Instance.ShowUI(E_UIType.PanelTestLogin,typeof(UILogin),null);
//				UIMgr.Instance.ShowUIAndCloseOthers(E_UIType.PanelTestLogin,typeof(UILogin),null);
				break;
			case "BtnMail":
				UIMgr.Instance.ShowUI(E_UIType.PanelTestMail,typeof(UIMail),null);
//				UIMgr.Instance.ShowUIAndCloseOthers(E_UIType.PanelTestMail,typeof(UIMail),null);
				break;
		}
	}

}
