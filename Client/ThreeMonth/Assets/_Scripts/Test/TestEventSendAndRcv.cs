/*******************************************************
 * 
 * 文件名(File Name)：             SpriteTitleChange.cs
 *
 * 作者(Author)：                  http://www.youkexueyuan.com
 *								  XiaoHong 
 *                                Yangzj
 *
 * 创建时间(CreateTime):           2016/03/01 14:48:33
 *
 *******************************************************/

using UnityEngine;
using System.Collections;
using MyFrameWork;

public class TestEventSendAndRcv : MonoBehaviour 
{
	void OnEnable()
	{
		EventDispatcher.AddListener<int>(EventDef.TestEvent.Test1,OnRecMsg);
	}

	void OnDisable()
	{
		EventDispatcher.RemoveListener<int>(EventDef.TestEvent.Test1,OnRecMsg);
	}

	private void OnRecMsg(int val)
	{
		DebugUtil.Info("OnRecMsg:" + val);
	}

	void OnGUI()
	{
		if(GUI.Button(new Rect(100,300,200,60),"发送消息，int：1024"))
		{
			EventDispatcher.TriggerEvent<int>(EventDef.TestEvent.Test1,1024);
		}
	}
}
