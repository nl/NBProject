/*******************************************************
 * 
 * 文件名(File Name)：             TestOpenUI
 *
 * 作者(Author)：                  Yangzj
 *
 * 创建时间(CreateTime):           2016/02/29 18:18:13
 *
 *******************************************************/

using UnityEngine;
using System.Collections;
using MyFrameWork;

public class TestOpenUI : MonoBehaviour 
{

	// Use this for initialization
	void Start () 
	{
		UIMgr.Instance.ShowUI(E_UIType.PanelTestMain,typeof(UIMain),OnUILoaded);
	}

	void OnUILoaded(BaseUI ui)
	{
		DebugUtil.Debug("ui loaded:" + ui.GetUIType());
	}

}
