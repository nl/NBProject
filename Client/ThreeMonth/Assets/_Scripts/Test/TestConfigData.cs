/*******************************************************
 * 
 * 文件名(File Name)：             SpriteTitleChange.cs
 *
 * 作者(Author)：                  http://www.youkexueyuan.com
 *								  XiaoHong 
 *                                Yangzj
 *
 * 创建时间(CreateTime):           2016/03/07 20:24:08
 *
 *******************************************************/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MyFrameWork;

namespace TDStruct
{
	public enum EnumSample
	{
		A = 1,
		B,
		C,
		D,
	}
}
public class TestConfigData : MonoBehaviour 
{
	// Use this for initialization
	void Start () 
	{
		DebugUtil.Debug("================ Card Table ================");

		List<int> listKeys = new List<int>(CardDataMgr.Instance.DicItem.Keys);
		for(int i = 0;i < listKeys.Count;i++)
		{
			var key = listKeys[i];
			DebugUtil.Info("keys:" + key + " remark:" + CardDataMgr.Instance.DicItem[key].Remark);
		}

		DebugUtil.Debug("================ Test Table ================");

		listKeys = new List<int>(TestTableMgr.Instance.DicItem.Keys);
		for(int i = 0;i < listKeys.Count && i < 10;i++)
		{
			var key = listKeys[i];
			string strFloatArr = string.Empty;
			foreach(var item in TestTableMgr.Instance.DicItem[key].Type_2)
			{
				strFloatArr += item.ToString("0.##") + ",";
			}
			DebugUtil.Info("keys:" + key + " float arr:" + strFloatArr.TrimEnd(',') + " enum:" + TestTableMgr.Instance.DicItem[key].Type_3.ToString());
		}
	}
}
