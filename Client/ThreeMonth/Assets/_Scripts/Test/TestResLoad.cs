/*******************************************************
 * 
 * 文件名(File Name)：             TestResLoad
 *
 * 作者(Author)：                  Yangzj
 *
 * 创建时间(CreateTime):           2016/02/24 18:09:25
 *
 *******************************************************/

using UnityEngine;
using System.Collections;
using MyFrameWork;

public class TestResLoad : MonoBehaviour 
{
	// Use this for initialization
	void Start () 
	{
		ResourceMgr.Instance.LoadAssetAndInstance("Test_dragon",OnLoadDragon);
	}

	public void OnLoadDragon(object goIns)
	{
		GameObject go = goIns as GameObject;
		DebugUtil.Debug("OnLoadDragon:" + go.name);
	}
		

}
