/*******************************************************
 * 
 * 文件名(File Name)：             TestMVC
 *
 * 作者(Author)：                  http://www.youkexueyuan.com
 *								  XiaoHong 
 *                                Yangzj
 *
 * 创建时间(CreateTime):           2016/03/10 15:56:14
 *
 *******************************************************/

using UnityEngine;
using System.Collections;
using MyFrameWork;

public class TestMVC : MonoBehaviour 
{
	// Use this for initialization
	void Start () 
	{
		UIMgr.Instance.ShowUI(E_UIType.PanelTestTopBar,typeof(UITopBar));
	}
}
