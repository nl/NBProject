/*******************************************************
 * 
 * 文件名(File Name)：             TestLog
 *
 * 作者(Author)：                  Yangzj
 *
 * 创建时间(CreateTime):           2016/02/25 00:36:03
 *
 *******************************************************/

using UnityEngine;
using System.Collections;

public class TestLog : MonoBehaviour 
{
	private TestLog1 _testLog = new TestLog1();
	// Use this for initialization
	void Start () 
	{
		Test1();

		DebugUtil.Warning("start");
	}

	private void Test1()
	{
		_testLog.Test();
	}

	// Update is called once per frame
	void Update () 
	{
		if(Input.GetKeyDown(KeyCode.A))
		{
			DebugUtil.Error("error");
		}
		else if(Input.GetKeyDown(KeyCode.B))
		{
			DebugUtil.Except(new System.Exception("aaa"));
		}
		else if(Input.GetKeyDown(KeyCode.C))
		{
			DebugUtil.Critical("critical msg");
		}
	}
}

public class TestLog1
{

	public void Test()
	{
		DebugUtil.Info("start:" + UnityEngine.Application.persistentDataPath);
	}
}
