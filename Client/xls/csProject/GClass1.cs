using System;
using System.Collections.Generic;
using System.IO;
public class GClass1 : GInterface1
{
	private string string_0;
	private int int_0 = 82305;
	private System.IO.BinaryWriter binaryWriter_0 = null;
	private static System.IO.FileStream fileStream_0 = null;
	private System.Collections.Generic.List<object> list_0 = new System.Collections.Generic.List<object>();
	public string imethod_0()
	{
		return this.string_0;
	}
	public void imethod_1(string string_1)
	{
		this.string_0 = string_1;
	}
	public GClass1(string string_1)
	{
		this.imethod_2(string_1);
	}
	public void imethod_2(string string_1)
	{
		this.imethod_1(string_1);
	}
	public bool method_0()
	{
		bool result;
		if (string.IsNullOrEmpty(this.imethod_0()))
		{
			result = false;
		}
		else
		{
			if (GClass1.fileStream_0 == null && this.binaryWriter_0 == null)
			{
				if (System.IO.File.Exists(this.imethod_0()))
				{
					System.IO.File.Delete(this.imethod_0());
				}
				GClass1.fileStream_0 = new System.IO.FileStream(this.imethod_0(), System.IO.FileMode.Create);
				this.binaryWriter_0 = new System.IO.BinaryWriter(GClass1.fileStream_0);
			}
			result = true;
		}
		return result;
	}
	private bool method_1(GClass6 gclass6_0, int int_1)
	{
		bool result;
		if (!this.method_0())
		{
			result = false;
		}
		else
		{
			if (this.binaryWriter_0 != null)
			{
			}
			this.binaryWriter_0.Write(this.int_0);
			this.binaryWriter_0.Write(int_1);
			this.binaryWriter_0.Write(gclass6_0.method_2());
			for (int i = 0; i < gclass6_0.method_2(); i++)
			{
				this.binaryWriter_0.Write(gclass6_0.method_5(i).string_6);
			}
			for (int i = 0; i < gclass6_0.method_2(); i++)
			{
				this.binaryWriter_0.Write((int)gclass6_0.method_5(i).genum1_0);
			}
			result = true;
		}
		return result;
	}
	private bool method_2(GClass5[] gclass5_0, GClass6 gclass6_0)
	{
		bool result;
		if (!this.method_0())
		{
			result = false;
		}
		else
		{
			for (int i = 0; i < gclass5_0.Length; i++)
			{
				GClass5 gClass = gclass5_0[i];
				for (int j = 0; j < gclass6_0.method_2(); j++)
				{
					GClass4 gClass2 = gclass6_0.method_5(j);
					switch (gClass2.genum1_0)
					{
					case GEnum1.Type_Enum:
					case GEnum1.Type_Int:
						this.binaryWriter_0.Write((int)gClass.method_0(j).object_0);
						break;
					case GEnum1.Type_Float:
						this.binaryWriter_0.Write((float)gClass.method_0(j).object_0);
						break;
					case GEnum1.Type_String:
						this.binaryWriter_0.Write(gClass.method_0(j).object_0.ToString());
						break;
					case GEnum1.Type_ObjArr:
						this.binaryWriter_0.Write((int)gClass.method_0(j).object_0);
						break;
					case GEnum1.Type_IntArr:
					{
						object[] object_ = gClass.method_0(j).object_1;
						if (object_ == null)
						{
							this.binaryWriter_0.Write(0);
						}
						else
						{
							this.binaryWriter_0.Write((short)object_.Length);
							for (int k = 0; k < object_.Length; k++)
							{
								this.binaryWriter_0.Write((int)object_[k]);
							}
						}
						break;
					}
					case GEnum1.Type_FloatArr:
					{
						object[] object_ = gClass.method_0(j).object_1;
						if (object_ == null)
						{
							this.binaryWriter_0.Write(0);
						}
						else
						{
							this.binaryWriter_0.Write((short)object_.Length);
							for (int k = 0; k < object_.Length; k++)
							{
								this.binaryWriter_0.Write((float)object_[k]);
							}
						}
						break;
					}
					}
				}
			}
			result = true;
		}
		return result;
	}
	private void method_3(string string_1)
	{
		string[] array = string_1.Split(new char[]
		{
			','
		});
		for (int i = 0; i < array.Length; i++)
		{
			if (this.method_4(array[i]))
			{
				char[] array2 = new char[array[i].Length - 7];
				for (int j = 6; j < array[i].Length - 1; j++)
				{
					array2[j - 6] = array[i][j];
				}
				this.method_3(new string(array2));
			}
			else
			{
				try
				{
					int num = System.Convert.ToInt32(array[i]);
					this.list_0.Add(num);
				}
				catch
				{
					try
					{
						double num2 = System.Convert.ToDouble(array[i]);
						this.list_0.Add((float)num2);
					}
					catch
					{
						string item = array[i];
						this.list_0.Add(item);
					}
				}
			}
		}
	}
	private bool method_4(string string_1)
	{
		return string_1 != null && string_1.Length >= 7 && (string_1[0] == 'u' && string_1[1] == 'n' && string_1[2] == 'i' && string_1[3] == 't' && string_1[4] == 'e' && string_1[5] == '(') && string_1[string_1.Length - 1] == ')';
	}
	public bool imethod_3(GClass6 gclass6_0, GClass5[] gclass5_0)
	{
		bool result;
		if (!this.method_1(gclass6_0, gclass5_0.Length))
		{
			result = false;
		}
		else
		{
			bool flag;
			if (flag = this.method_2(gclass5_0, gclass6_0))
			{
				GClass1.fileStream_0.Close();
				this.binaryWriter_0.Close();
				GClass1.fileStream_0 = null;
				this.binaryWriter_0 = null;
			}
			result = flag;
		}
		return result;
	}
}
