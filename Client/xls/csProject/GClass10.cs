using System;
using System.Collections.Generic;
public class GClass10
{
	public System.Collections.Generic.List<GClass5> list_0 = new System.Collections.Generic.List<GClass5>();
	public GClass6 gclass6_0 = null;
	private System.Collections.Generic.Dictionary<int, int> dictionary_0 = new System.Collections.Generic.Dictionary<int, int>();
	public int method_0()
	{
		return this.list_0.Count;
	}
	public int method_1()
	{
		return this.gclass6_0.method_2();
	}
	public GClass10(GInterface0 ginterface0_0)
	{
		this.gclass6_0 = ginterface0_0.imethod_4();
		GClass5[] array = ginterface0_0.imethod_3();
		for (int i = 0; i < array.Length; i++)
		{
			GClass5 gClass = array[i];
			this.list_0.Add(gClass);
			//Class1.smethod_2(((int)gClass.list_0[0].object_0).ToString());
			this.dictionary_0.Add((int)gClass.list_0[0].object_0, this.list_0.Count - 1);
		}
	}
	public GClass3 method_2(int int_0, int int_1)
	{
		GClass5 gClass = this.method_4(int_0);
		GClass3 result;
		if (gClass != null)
		{
			result = gClass.method_0(int_1);
		}
		else
		{
			result = null;
		}
		return result;
	}
	public GClass3 method_3(int int_0, string string_0)
	{
		GClass5 gClass = this.method_4(int_0);
		GClass3 result;
		if (gClass != null)
		{
			result = gClass.method_1(string_0);
		}
		else
		{
			result = null;
		}
		return result;
	}
	public GClass5 method_4(int int_0)
	{
		int index = 0;
		GClass5 result;
		if (this.dictionary_0.TryGetValue(int_0, out index))
		{
			result = this.list_0[index];
		}
		else
		{
			result = null;
		}
		return result;
	}
	public GClass5[] method_5()
	{
		return this.list_0.ToArray();
	}
	public void method_6(int int_0, int int_1, object object_0)
	{
		GClass3 gClass = this.method_2(int_0, int_1);
		gClass.object_0 = object_0;
	}
	public GClass5 method_7(int int_0, object object_0)
	{
		GClass5 gClass;
		GClass5 result;
		foreach (GClass5 current in this.list_0)
		{
			switch (this.gclass6_0.method_5(int_0).genum1_0)
			{
			case GEnum1.Type_Int:
			case GEnum1.Type_Enum:
				if ((int)object_0 == (int)current.list_0[int_0].object_0)
				{
					gClass = current;
					result = gClass;
					return result;
				}
				break;
			case GEnum1.Type_Float:
				if ((float)object_0 == (float)current.list_0[int_0].object_0)
				{
					gClass = current;
					result = gClass;
					return result;
				}
				break;
			case GEnum1.Type_String:
				if ((string)object_0 == (string)current.list_0[int_0].object_0)
				{
					gClass = current;
					result = gClass;
					return result;
				}
				break;
			case GEnum1.Type_ObjArr:
				if ((int)object_0 == (int)current.list_0[int_0].object_0)
				{
					gClass = current;
					result = gClass;
					return result;
				}
				break;
			}
		}
		gClass = null;
		result = gClass;
		return result;
	}
	public string method_8(int int_0)
	{
		return this.gclass6_0.method_5(int_0).string_6;
	}
	public GEnum1 method_9(int int_0)
	{
		return this.gclass6_0.method_5(int_0).genum1_0;
	}
	public bool method_10(GInterface1 ginterface1_0)
	{
		return ginterface1_0.imethod_3(this.gclass6_0, this.list_0.ToArray());
	}
	public T method_11<T>(int int_0) where T : GInterface2, new()
	{
		GClass5 gClass = this.method_4(int_0);
		T result = (default(T) == null) ? System.Activator.CreateInstance<T>() : default(T);
		if (gClass != null)
		{
			result.imethod_0(gClass);
		}
		return result;
	}
}
