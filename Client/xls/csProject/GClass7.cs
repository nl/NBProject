using System;
public class GClass7<T> where T : new()
{
	protected static T gparam_0 = (default(T) == null) ? System.Activator.CreateInstance<T>() : default(T);
	public static T smethod_0()
	{
		return GClass7<T>.gparam_0;
	}
	protected GClass7()
	{
	}
}
