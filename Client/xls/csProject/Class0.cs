using System;
using System.IO;
using System.Windows.Forms;
internal static class Class0
{
	private static string string_0 = Application.StartupPath + "\\config.txt";
	private static int rowEnglishName = 3; //程序中字段名,所在行索引
	private static int rowChineseName = 0;//字段意义中文行索引
	private static int rowVariableType = 1;//字段类型行索引
	private static int rowDataBegin = 4; //数据开始行索引
	public static string string_1;
	public static string string_2;
	public static string string_3;
	public static int smethod_0()
	{
		return Class0.rowVariableType;
	}
	public static void smethod_1(int int_4)
	{
		Class0.rowVariableType = int_4;
	}
	public static int smethod_2()
	{
		return Class0.rowChineseName;
	}
	public static void smethod_3(int int_4)
	{
		Class0.rowChineseName = int_4;
	}
	public static int smethod_4()
	{
		return Class0.rowEnglishName;
	}
	public static void smethod_5(int int_4)
	{
		Class0.rowEnglishName = int_4;
	}
	public static int smethod_6()
	{
		return Class0.rowDataBegin;
	}
	public static void smethod_7(int int_4)
	{
		Class0.rowDataBegin = int_4;
	}
	public static string smethod_8()
	{
		Class1.smethod_2(Class0.string_0, System.ConsoleColor.White);
		return Class0.string_0;
	}
	public static void smethod_9(string string_4)
	{
		Class0.string_0 = string_4;
	}
	public static string smethod_10()
	{
		return Class0.string_1;
	}
	public static void smethod_11(string string_4)
	{
		Class0.string_1 = string_4;
	}
	public static string smethod_12()
	{
		return Class0.string_2;
	}
	public static void smethod_13(string string_4)
	{
		Class0.string_2 = string_4;
	}
	public static string smethod_14()
	{
		return Class0.string_3;
	}
	public static void smethod_15(string string_4)
	{
		Class0.string_3 = string_4;
	}
	public static void smethod_16(string string_4)
	{
		if (!System.IO.File.Exists(string_4))
		{
			Class1.smethod_0("first create " + GClass11.smethod_2(string_4));
		}
		using (System.IO.FileStream fileStream = new System.IO.FileStream(string_4, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.ReadWrite))
		{
			using (System.IO.StreamReader streamReader = new System.IO.StreamReader(fileStream))
			{
				Class0.string_1 = streamReader.ReadLine();
				Class0.string_3 = streamReader.ReadLine();
				Class0.string_2 = streamReader.ReadLine();
			}
		}
	}
	public static void smethod_17()
	{
		using (System.IO.FileStream fileStream = new System.IO.FileStream(Class0.smethod_8(), System.IO.FileMode.Create))
		{
			using (System.IO.StreamWriter streamWriter = new System.IO.StreamWriter(fileStream))
			{
				streamWriter.WriteLine(Class0.string_1);
				streamWriter.WriteLine(Class0.string_3);
				streamWriter.WriteLine(Class0.string_2);
				Class1.smethod_2("save excel表格路径:" + Class0.string_1, System.ConsoleColor.White);
				Class1.smethod_2("save 代码输出路径:" + Class0.string_3, System.ConsoleColor.White);
				Class1.smethod_2("save 资源输出路径:" + Class0.string_2, System.ConsoleColor.White);
				Class1.smethod_0("保存配置成功");
			}
		}
	}
}
