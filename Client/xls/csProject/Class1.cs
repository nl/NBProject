using System;
internal static class Class1
{
	public static void smethod_0(string string_0)
	{
		Class1.smethod_2(string_0, System.ConsoleColor.Yellow);
	}
	public static void smethod_1(string string_0)
	{
		Class1.smethod_2(string_0, System.ConsoleColor.Red);
		throw new Exception0(string_0);
	}
	public static void smethod_2(string string_0, System.ConsoleColor consoleColor_0 = System.ConsoleColor.White)
	{
		string value = System.DateTime.Now.ToLocalTime().ToString() + "    " + string_0;
		System.Console.ForegroundColor = consoleColor_0;
		System.Console.WriteLine(value);
	}
}
