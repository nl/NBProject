using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
public class GClass2 : GInterface0
{
	private string string_0;
	private string string_1;
	private HSSFWorkbook hssfworkbook_0 = null;
	private XSSFWorkbook xssfworkbook_0 = null;
	private System.Collections.Generic.List<GClass5> list_0 = new System.Collections.Generic.List<GClass5>();
	private GClass6 gclass6_0 = null;
	public string imethod_0()
	{
		return this.string_0;
	}
	public void imethod_1(string string_2)
	{
		this.string_0 = string_2;
	}
	public GClass2(string string_2)
	{
		this.imethod_2(string_2);
	}
	public void imethod_2(string string_2)
	{
		try
		{
			this.imethod_1(string_2);
			string[] array = this.imethod_0().Split(new char[]
			{
				'/'
			});
			string text = array[array.Length - 1];
			string[] array2 = text.Split(new char[]
			{
				'.'
			});
			this.string_1 = array2[0];
			if (array2[1] == "xlsx")
			{
				this.method_0(true);
				this.method_1(true);
				this.method_2(true);
			}
			else
			{
				if (array2[1] == "xls")
				{
					this.method_0(false);
					this.method_1(false);
					this.method_2(false);
				}
			}
		}
		catch (Exception0 exception)
		{
			throw exception;
		}
		catch (System.Exception)
		{
			throw new Exception0("error, table : " + GClass7<GClass9>.smethod_0().string_0);
		}
	}
	private void method_0(bool bool_0)
	{
		using (System.IO.FileStream fileStream = new System.IO.FileStream(this.imethod_0(), System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.ReadWrite))
		{
			if (bool_0)
			{
				this.xssfworkbook_0 = new XSSFWorkbook(fileStream);
			}
			else
			{
				this.hssfworkbook_0 = new HSSFWorkbook(fileStream);
			}
		}
	}
	private void method_1(bool bool_0)
	{
		this.gclass6_0 = new GClass6();
		this.gclass6_0.method_1(this.string_1);
		ISheet sheetAt;
		if (bool_0)
		{
			sheetAt = this.xssfworkbook_0.GetSheetAt(0);
		}
		else
		{
			sheetAt = this.hssfworkbook_0.GetSheetAt(0);
		}
		IRow row = sheetAt.GetRow(Class0.smethod_4());
		IRow row2 = sheetAt.GetRow(Class0.smethod_2());
		IRow row3 = sheetAt.GetRow(Class0.smethod_0());
		for (int i = 0; i < (int)row.LastCellNum; i++)
		{
			GClass4 gClass = new GClass4();
			gClass.string_6 = row.GetCell(i).ToString();
			if (string.IsNullOrEmpty(gClass.string_6.Trim()))
			{
				Class1.smethod_1(this.string_1 + "列:" + i.ToString() + "列名为空");
				return;
			}
			gClass.int_0 = i;
			string string_ = row3.GetCell(i).ToString();
			gClass.method_4(string_);
			gClass.method_3(row2.GetCell(i).ToString());
			this.gclass6_0.method_3(gClass);
		}
	}

	private void method_2(bool bool_0)
	{
		if (this.gclass6_0 != null)
		{
			ISheet sheetAt;
			if (bool_0)
			{
				sheetAt = this.xssfworkbook_0.GetSheetAt(0);
			}
			else
			{
				sheetAt = this.hssfworkbook_0.GetSheetAt(0);
			}
			int num = 0;
			System.Collections.IEnumerator rowEnumerator = sheetAt.GetRowEnumerator();
			while (rowEnumerator.MoveNext())
			{
				IRow row = (IRow)rowEnumerator.Current;
				if (num < Class0.smethod_6())
				{
					num++;
				}
				else
				{
					GClass5 gClass = new GClass5(this.gclass6_0);
					for (int i = 0; i < this.gclass6_0.method_2(); i++)
					{
						ICell cell = row.GetCell(i);
						GClass4 gclass4_ = this.gclass6_0.method_5(i);
						GClass3 item = this.method_9(cell, gclass4_);
						gClass.list_0.Add(item);
					}
					this.list_0.Add(gClass);
					num++;
				}
			}
		}
		else
		{
			Class1.smethod_1("m_Schema is null");
		}
	}
	private System.Collections.Generic.List<object> method_3(ICell icell_0, string string_2)
	{
		System.Collections.Generic.List<object> list = new System.Collections.Generic.List<object>();
		string text = this.method_5(icell_0, string_2);
		try
		{
			string[] array = text.Split(new char[]
			{
				','
			});
			for (int i = 0; i < array.Length; i++)
			{
				list.Add(int.Parse(array[i]));
			}
		}
		catch (System.Exception)
		{
			this.method_6(icell_0, string_2);
		}
		return list;
	}
	private System.Collections.Generic.List<object> method_4(ICell icell_0, string string_2)
	{
		System.Collections.Generic.List<object> list = new System.Collections.Generic.List<object>();
		string text = this.method_5(icell_0, string_2);
		try
		{
			string[] array = text.Split(new char[]
			{
				','
			});
			for (int i = 0; i < array.Length; i++)
			{
				list.Add(float.Parse(array[i]));
			}
		}
		catch (System.Exception)
		{
			this.method_6(icell_0, string_2);
		}
		return list;
	}
	private string method_5(ICell icell_0, string string_2)
	{
		string result;
		try
		{
			if (CellType.Numeric == icell_0.CellType)
			{
				result = ((float)icell_0.NumericCellValue).ToString();
				return result;
			}
			result = icell_0.StringCellValue;
			return result;
		}
		catch (System.Exception)
		{
			this.method_6(icell_0, string_2);
		}
		result = "";
		return result;
	}
	private void method_6(ICell icell_0, string string_2)
	{
		throw new Exception0(string.Concat(new string[]
		{
			"表",
			GClass7<GClass9>.smethod_0().string_0,
			"\r\n列 ",
			string_2,
			"\r\n第",
			(icell_0.ColumnIndex + 1).ToString(),
			"列\r\n第",
			(icell_0.RowIndex + 1).ToString(),
			"行"
		}));
	}
	private int method_7(ICell icell_0, string string_2)
	{
		int result;
		try
		{
			if (CellType.String == icell_0.CellType)
			{
				string stringCellValue = icell_0.StringCellValue;
				result = int.Parse(stringCellValue);
				return result;
			}
			result = (int)icell_0.NumericCellValue;
			return result;
		}
		catch (System.Exception)
		{
			this.method_6(icell_0, string_2);
		}
		result = 0;
		return result;
	}
	private float method_8(ICell icell_0, string string_2)
	{
		float result;
		try
		{
			if (CellType.String == icell_0.CellType)
			{
				string stringCellValue = icell_0.StringCellValue;
				result = float.Parse(stringCellValue);
				return result;
			}
			result = (float)icell_0.NumericCellValue;
			return result;
		}
		catch (System.Exception)
		{
			this.method_6(icell_0, string_2);
		}
		result = 0f;
		return result;
	}
	private GClass3 method_9(ICell icell_0, GClass4 gclass4_0)
	{
		GClass3 result = null;
		if (icell_0 != null)
		{
			switch (gclass4_0.genum1_0)
			{
			case GEnum1.Type_Enum:
			case GEnum1.Type_Int:
				result = new GClass3(this.method_7(icell_0, gclass4_0.string_6));
				break;
			case GEnum1.Type_Float:
				result = new GClass3(this.method_8(icell_0, gclass4_0.string_6));
				break;
			case GEnum1.Type_String:
				result = new GClass3(this.method_5(icell_0, gclass4_0.string_6));
				break;
			case GEnum1.Type_ObjArr:
			{
				int num = (int)icell_0.NumericCellValue;
				result = new GClass3(num);
				break;
			}
			case GEnum1.Type_IntArr:
			{
				System.Collections.Generic.List<object> list = this.method_3(icell_0, gclass4_0.string_6);
				result = new GClass3(list.ToArray());
				break;
			}
			case GEnum1.Type_FloatArr:
			{
				System.Collections.Generic.List<object> list2 = this.method_4(icell_0, gclass4_0.string_6);
				result = new GClass3(list2.ToArray());
				break;
			}
			}
		}
		else
		{
			switch (gclass4_0.genum1_0)
			{
			case GEnum1.Type_Enum:
			case GEnum1.Type_Int:
				result = new GClass3(0);
				break;
			case GEnum1.Type_Float:
				result = new GClass3(0f);
				break;
			case GEnum1.Type_String:
				result = new GClass3("");
				break;
			case GEnum1.Type_ObjArr:
				result = new GClass3(0);
				break;
			case GEnum1.Type_IntArr:
				result = new GClass3(null);
				break;
			case GEnum1.Type_FloatArr:
				result = new GClass3(null);
				break;
			}
		}
		return result;
	}
	public GClass6 imethod_4()
	{
		return this.gclass6_0;
	}
	public GClass5[] imethod_3()
	{
		GClass5[] result;
		if (this.list_0.Count > 0)
		{
			result = this.list_0.ToArray();
		}
		else
		{
			result = null;
		}
		return result;
	}
}
