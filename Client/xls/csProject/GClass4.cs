using System;
public class GClass4
{
	public static string string_0 = "enum";
	public static string string_1 = "int";
	public static string string_2 = "string";
	public static string string_3 = "float";
	public static string string_4 = "intarr";
	public static string string_5 = "floatarr";
	public string string_6;
	private string string_7 = "";
	private string string_8 = "";
	public GEnum1 genum1_0;
	public int int_0;
	public string method_0()
	{
		return this.string_8;
	}
	public void method_1(string string_9)
	{
		this.string_8 = string_9;
	}
	public string method_2()
	{
		return this.string_7;
	}
	public void method_3(string string_9)
	{
		this.string_7 = string_9;
	}
	public void method_4(string string_9)
	{
		string text = string_9.ToLower();
		if (GClass4.string_1 == text)
		{
			this.genum1_0 = GEnum1.Type_Int;
		}
		else
		{
			if (GClass4.string_3 == text)
			{
				this.genum1_0 = GEnum1.Type_Float;
			}
			else
			{
				if (GClass4.string_2 == text)
				{
					this.genum1_0 = GEnum1.Type_String;
				}
				else
				{
					if (GClass4.string_4 == text)
					{
						this.genum1_0 = GEnum1.Type_IntArr;
					}
					else
					{
						if (GClass4.string_5 == text)
						{
							this.genum1_0 = GEnum1.Type_FloatArr;
						}
						else
						{
							if (text.Contains("enum_"))
							{
								this.genum1_0 = GEnum1.Type_Enum;
								this.string_8 = string_9.Split(new char[]
								{
									'_'
								})[1];
							}
							else
							{
								if (text == "UNITE" || text == "unite" || text == "Unite")
								{
									this.genum1_0 = GEnum1.Type_ObjArr;
								}
								else
								{
									this.genum1_0 = GEnum1.Type_UnKnown;
								}
							}
						}
					}
				}
			}
		}
	}
}
