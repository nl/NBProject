using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
public class ExcelToCode : Form
{
	private bool bool_0 = true;
	private IContainer icontainer_0 = null;
	private Button btn_createconfig;
	private Button btn_create;
	private Button btn_selectFolder;
	private Label label1;
	private TextBox txt_path;
	private FolderBrowserDialog folderBrowserDialog_0;
	private Button btn_output;
	private Label lab_outpath;
	private TextBox txt_output;
	private Label label2;
	private TextBox tbxcodepath;
	private Button btn_codepath;
	public ExcelToCode()
	{
		this.InitializeComponent();
		string currentPath = new System.IO.DirectoryInfo(System.IO.Path.GetFullPath(Application.ExecutablePath) + "\\..").FullName;//new System.IO.DirectoryInfo("..").FullName;
		//string currentPath = System.IO.Path.GetFullPath(Application.ExecutablePath);
		string strXlsPath = new System.IO.DirectoryInfo(currentPath + "\\..").FullName;
		string strBinaryPath = new System.IO.DirectoryInfo(currentPath + "\\..\\..\\ThreeMonth\\Assets\\ResourcesRaw\\ConfigDataTable").FullName;
		string strCsPath = new System.IO.DirectoryInfo(currentPath + "\\..\\..\\ThreeMonth\\Assets\\_Scripts\\GameLogic\\ConfigData").FullName;

		this.txt_path.Text = ((Class0.smethod_10() == null) ? strXlsPath : Class0.smethod_10());
		Class0.string_1 = this.txt_path.Text;

		this.txt_output.Text = ((Class0.smethod_12() == null) ? strBinaryPath : Class0.smethod_12());
		Class0.string_2 = this.txt_output.Text;

		this.tbxcodepath.Text = ((Class0.smethod_14() == null) ? strCsPath : Class0.smethod_14());
		Class0.string_3 = this.tbxcodepath.Text;

//		if (!string.IsNullOrEmpty(Class0.smethod_10()) && !string.IsNullOrEmpty(Class0.smethod_12()) && !string.IsNullOrEmpty(Class0.smethod_14()))
//		{
//			this.bool_0 = false;
//			this.btn_create_Click(null, null);
//			System.Environment.Exit(0);
//		}
	}
	private void btn_selectFolder_Click(object sender, System.EventArgs e)
	{
		this.folderBrowserDialog_0.ShowDialog();
		this.txt_path.Text = this.folderBrowserDialog_0.SelectedPath;
		Class0.smethod_11(this.txt_path.Text.Trim());
	}
	private void btn_create_Click(object sender, System.EventArgs e)
	{
		try
		{
			if (!this.method_1())
			{
				Class1.smethod_1("请指定Excel目录");
			}
			else
			{
				if (!this.method_2())
				{
					Class1.smethod_1("请指定资源输出目录");
				}
				else
				{
					if (!this.method_3())
					{
						Class1.smethod_1("请指定代码输出目录");
					}
					this.method_0();
				}
			}
		}
		catch (Exception0 exception)
		{
			MessageBox.Show(exception.Message);
		}
		catch (System.Exception ex)
		{
			MessageBox.Show(GClass7<GClass9>.smethod_0().string_0 + ex.ToString());
		}
	}
	private void method_0()
	{
		string[] array = GClass11.smethod_5(Class0.smethod_10(), "xls;xlsx", true);
		string[] array2 = array;
		for (int i = 0; i < array2.Length; i++)
		{
			string text = array2[i];
			string string_ = GClass11.smethod_2(text);
			GClass11.smethod_3(text);
			GClass7<GClass9>.smethod_0().method_0(text);
			GClass7<GClass9>.smethod_0().method_2(string_, Class0.smethod_12() + "//", GEnum0.const_1);
			GClass7<GClass9>.smethod_0().method_5(string_, Class0.smethod_14() + "//");
		}
		Class1.smethod_2("生成完成", System.ConsoleColor.White);
		if (this.bool_0)
		{
			MessageBox.Show("生成完成");
		}
//		Application.Exit();
	}
	private bool method_1()
	{
		return !string.IsNullOrEmpty(Class0.smethod_10());
	}
	private bool method_2()
	{
		return !string.IsNullOrEmpty(Class0.smethod_12());
	}
	private bool method_3()
	{
		return !string.IsNullOrEmpty(Class0.smethod_14());
	}
	private void btn_output_Click(object sender, System.EventArgs e)
	{
		this.folderBrowserDialog_0.ShowDialog();
		Class0.smethod_13(this.folderBrowserDialog_0.SelectedPath);
		this.txt_output.Text = Class0.smethod_12();
	}
	private void btn_createconfig_Click(object sender, System.EventArgs e)
	{
		Class0.smethod_17();
	}
	private void btn_codepath_Click(object sender, System.EventArgs e)
	{
		this.folderBrowserDialog_0.ShowDialog();
		Class0.smethod_15(this.folderBrowserDialog_0.SelectedPath);
		this.tbxcodepath.Text = Class0.smethod_14();
	}
	protected override void Dispose(bool disposing)
	{
		if (disposing && this.icontainer_0 != null)
		{
			this.icontainer_0.Dispose();
		}
		base.Dispose(disposing);
	}
	private void InitializeComponent()
	{
		this.btn_createconfig = new Button();
		this.btn_create = new Button();
		this.btn_selectFolder = new Button();
		this.label1 = new Label();
		this.txt_path = new TextBox();
		this.folderBrowserDialog_0 = new FolderBrowserDialog();
		this.btn_output = new Button();
		this.lab_outpath = new Label();
		this.txt_output = new TextBox();
		this.label2 = new Label();
		this.tbxcodepath = new TextBox();
		this.btn_codepath = new Button();
		base.SuspendLayout();
		this.btn_createconfig.Location = new Point(85, 203);
		this.btn_createconfig.Name = "btn_createconfig";
		this.btn_createconfig.Size = new Size(75, 23);
		this.btn_createconfig.TabIndex = 0;
		this.btn_createconfig.Text = "生成配置";
		this.btn_createconfig.UseVisualStyleBackColor = true;
		this.btn_createconfig.Click += new System.EventHandler(this.btn_createconfig_Click);
		this.btn_create.Location = new Point(85, 156);
		this.btn_create.Name = "btn_create";
		this.btn_create.Size = new Size(75, 23);
		this.btn_create.TabIndex = 1;
		this.btn_create.Text = "生成";
		this.btn_create.UseVisualStyleBackColor = true;
		this.btn_create.Click += new System.EventHandler(this.btn_create_Click);
		this.btn_selectFolder.Location = new Point(233, 21);
		this.btn_selectFolder.Name = "btn_selectFolder";
		this.btn_selectFolder.Size = new Size(75, 23);
		this.btn_selectFolder.TabIndex = 2;
		this.btn_selectFolder.Text = "浏览目录";
		this.btn_selectFolder.UseVisualStyleBackColor = true;
		this.btn_selectFolder.Click += new System.EventHandler(this.btn_selectFolder_Click);
		this.label1.AutoSize = true;
		this.label1.Location = new Point(30, 26);
		this.label1.Name = "label1";
		this.label1.Size = new Size(59, 12);
		this.label1.TabIndex = 3;
		this.label1.Text = "Excel目录";
		this.txt_path.Location = new Point(105, 23);
		this.txt_path.Name = "txt_path";
		this.txt_path.ReadOnly = true;
		this.txt_path.Size = new Size(122, 21);
		this.txt_path.TabIndex = 4;
		this.btn_output.Location = new Point(233, 95);
		this.btn_output.Name = "btn_output";
		this.btn_output.Size = new Size(75, 23);
		this.btn_output.TabIndex = 5;
		this.btn_output.Text = "浏览目录";
		this.btn_output.UseVisualStyleBackColor = true;
		this.btn_output.Click += new System.EventHandler(this.btn_output_Click);
		this.lab_outpath.AutoSize = true;
		this.lab_outpath.Location = new Point(12, 100);
		this.lab_outpath.Name = "lab_outpath";
		this.lab_outpath.Size = new Size(77, 12);
		this.lab_outpath.TabIndex = 6;
		this.lab_outpath.Text = "资源输出目录";
		this.txt_output.Location = new Point(105, 97);
		this.txt_output.Name = "txt_output";
		this.txt_output.ReadOnly = true;
		this.txt_output.Size = new Size(122, 21);
		this.txt_output.TabIndex = 7;
		this.label2.AutoSize = true;
		this.label2.Location = new Point(12, 59);
		this.label2.Name = "label2";
		this.label2.Size = new Size(77, 12);
		this.label2.TabIndex = 8;
		this.label2.Text = "代码输出目录";
		this.tbxcodepath.Location = new Point(105, 56);
		this.tbxcodepath.Name = "tbxcodepath";
		this.tbxcodepath.ReadOnly = true;
		this.tbxcodepath.Size = new Size(122, 21);
		this.tbxcodepath.TabIndex = 1;
		this.btn_codepath.Location = new Point(233, 54);
		this.btn_codepath.Name = "btn_codepath";
		this.btn_codepath.Size = new Size(75, 23);
		this.btn_codepath.TabIndex = 0;
		this.btn_codepath.Text = "浏览目录";
		this.btn_codepath.Click += new System.EventHandler(this.btn_codepath_Click);
		base.AutoScaleDimensions = new SizeF(6f, 12f);
		base.AutoScaleMode = AutoScaleMode.Font;
		base.ClientSize = new Size(320, 262);
		base.Controls.Add(this.btn_codepath);
		base.Controls.Add(this.tbxcodepath);
		base.Controls.Add(this.label2);
		base.Controls.Add(this.txt_output);
		base.Controls.Add(this.lab_outpath);
		base.Controls.Add(this.btn_output);
		base.Controls.Add(this.txt_path);
		base.Controls.Add(this.label1);
		base.Controls.Add(this.btn_selectFolder);
		base.Controls.Add(this.btn_create);
		base.Controls.Add(this.btn_createconfig);
		base.Name = "ExcelToCode";
		this.Text = "ExcelToCode";
		base.ResumeLayout(false);
		base.PerformLayout();
	}
}
