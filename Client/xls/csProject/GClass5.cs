using System;
using System.Collections.Generic;
public class GClass5
{
	public GClass6 gclass6_0 = null;
	public System.Collections.Generic.List<GClass3> list_0 = new System.Collections.Generic.List<GClass3>();
	public GClass5(GClass6 gclass6_1)
	{
		this.gclass6_0 = gclass6_1;
	}
	public GClass3 method_0(int int_0)
	{
		GClass3 result = null;
		if (int_0 >= 0 && int_0 < this.list_0.Count)
		{
			result = this.list_0[int_0];
		}
		return result;
	}
	public GClass3 method_1(string string_0)
	{
		GClass4 gClass = this.gclass6_0.method_4(string_0);
		return this.method_0(gClass.int_0);
	}
}
