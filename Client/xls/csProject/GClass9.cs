using System;
using System.Collections.Generic;
public class GClass9 : GClass7<GClass9>
{
	private System.Collections.Generic.Dictionary<string, GClass10> dictionary_0 = new System.Collections.Generic.Dictionary<string, GClass10>();
	private System.Collections.Generic.Dictionary<string, string> dictionary_1 = new System.Collections.Generic.Dictionary<string, string>();
	public string string_0 = "";
	public bool method_0(string string_1)
	{
		this.string_0 = string_1;
		GInterface0 gInterface = GClass7<Class3>.smethod_0().method_0(string_1);
		GClass6 gClass = gInterface.imethod_4();
		bool result;
		if (gClass == null)
		{
			result = false;
		}
		else
		{
			GClass10 value = new GClass10(gInterface);
			if (this.dictionary_0.ContainsKey(gClass.method_0()))
			{
				this.dictionary_0[gClass.method_0()] = value;
			}
			else
			{
				this.dictionary_0.Add(gClass.method_0(), value);
			}
			if (this.dictionary_1.ContainsKey(gClass.method_0()))
			{
				this.dictionary_1[gClass.method_0()] = string_1;
			}
			else
			{
				this.dictionary_1.Add(gClass.method_0(), string_1);
			}
			result = true;
		}
		return result;
	}
	public bool method_1(string string_1, GEnum0 genum0_0)
	{
		string text = null;
		this.dictionary_1.TryGetValue(string_1, out text);
		bool result;
		if (text != null)
		{
			string[] array = text.Split(new char[]
			{
				'.'
			});
			text = array[0] + "." + GClass7<Class3>.smethod_0().method_1(genum0_0);
			result = this.method_3(string_1, text);
		}
		else
		{
			result = false;
		}
		return result;
	}
	public bool method_2(string string_1, string string_2, GEnum0 genum0_0)
	{
		return string_2 != null && this.method_3(string_1, string_2 + string_1 + "." + GClass7<Class3>.smethod_0().method_1(genum0_0));
	}
	public bool method_3(string string_1, string string_2)
	{
		GInterface1 ginterface1_ = GClass7<Class3>.smethod_0().method_2(string_2);
		GClass10 gClass = null;
		this.dictionary_0.TryGetValue(string_1, out gClass);
		return gClass != null && gClass.method_10(ginterface1_);
	}
	public GClass10 method_4(string string_1)
	{
		GClass10 result = null;
		this.dictionary_0.TryGetValue(string_1, out result);
		return result;
	}
	public void method_5(string string_1, string string_2)
	{
		GClass10 gClass = this.method_4(string_1);
		if (gClass != null)
		{
			GClass6 gclass6_ = gClass.gclass6_0;
			GClass7<GClass8>.smethod_0().method_0(gclass6_, string_2);
		}
	}
}
