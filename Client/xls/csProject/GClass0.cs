using System;
using System.Collections.Generic;
using System.IO;
public class GClass0 : GInterface0
{
	private string string_0;
	private string string_1;
	private int int_0 = 0;
	private int int_1 = 82305;
	private System.IO.BinaryReader binaryReader_0 = null;
	private static System.IO.FileStream fileStream_0 = null;
	private System.Collections.Generic.List<GClass5> list_0 = new System.Collections.Generic.List<GClass5>();
	private GClass6 gclass6_0 = null;
	private System.Collections.Generic.List<object> list_1 = new System.Collections.Generic.List<object>();
	public string imethod_0()
	{
		return this.string_0;
	}
	public void imethod_1(string string_2)
	{
		this.string_0 = string_2;
	}
	public GClass0(string string_2)
	{
		this.imethod_2(string_2);
	}
	public void imethod_2(string string_2)
	{
		this.imethod_1(string_2);
		string[] array = this.imethod_0().Split(new char[]
		{
			'/'
		});
		string text = array[array.Length - 1];
		string[] array2 = text.Split(new char[]
		{
			'.'
		});
		this.string_1 = array2[0];
		if (this.method_1())
		{
			this.method_2();
			this.method_3();
			this.method_0();
		}
	}
	private void method_0()
	{
		if (GClass0.fileStream_0 != null)
		{
			GClass0.fileStream_0.Close();
		}
		if (this.binaryReader_0 != null)
		{
			this.binaryReader_0.Close();
		}
	}
	public bool method_1()
	{
		bool result;
		if (string.IsNullOrEmpty(this.imethod_0()))
		{
			result = false;
		}
		else
		{
			if (!System.IO.File.Exists(this.imethod_0()))
			{
				result = false;
			}
			else
			{
				GClass0.fileStream_0 = new System.IO.FileStream(this.imethod_0(), System.IO.FileMode.Open);
				this.binaryReader_0 = new System.IO.BinaryReader(GClass0.fileStream_0);
				result = true;
			}
		}
		return result;
	}
	private void method_2()
	{
		if (this.gclass6_0 == null)
		{
			this.gclass6_0 = new GClass6();
			this.gclass6_0.method_1(this.string_1);
			int num = this.binaryReader_0.ReadInt32();
			if (this.int_1 == num)
			{
				this.int_0 = this.binaryReader_0.ReadInt32();
				int num2 = this.binaryReader_0.ReadInt32();
				for (int i = 0; i < num2; i++)
				{
					GClass4 gClass = new GClass4();
					gClass.string_6 = this.binaryReader_0.ReadString();
					gClass.int_0 = i;
					this.gclass6_0.method_3(gClass);
				}
				for (int i = 0; i < num2; i++)
				{
					GClass4 gClass = this.gclass6_0.method_5(i);
					gClass.genum1_0 = (GEnum1)this.binaryReader_0.ReadInt32();
				}
			}
		}
	}
	private void method_3()
	{
		if (this.gclass6_0 != null)
		{
			for (int i = 0; i < this.int_0; i++)
			{
				GClass5 gClass = new GClass5(this.gclass6_0);
				for (int j = 0; j < this.gclass6_0.method_2(); j++)
				{
					GClass4 gClass2 = this.gclass6_0.method_5(j);
					GClass3 item;
					switch (gClass2.genum1_0)
					{
					case GEnum1.Type_Enum:
					case GEnum1.Type_Int:
					{
						int num = this.binaryReader_0.ReadInt32();
						item = new GClass3(num);
						break;
					}
					case GEnum1.Type_Float:
					{
						float num2 = this.binaryReader_0.ReadSingle();
						item = new GClass3(num2);
						break;
					}
					case GEnum1.Type_String:
					{
						string object_ = this.binaryReader_0.ReadString();
						item = new GClass3(object_);
						break;
					}
					case GEnum1.Type_ObjArr:
					{
						string string_ = this.binaryReader_0.ReadString();
						this.list_1.Clear();
						this.method_4(string_);
						item = new GClass3(this.list_1.ToArray());
						break;
					}
					case GEnum1.Type_IntArr:
					{
						int num3 = (int)this.binaryReader_0.ReadInt16();
						System.Collections.Generic.List<int> list = new System.Collections.Generic.List<int>();
						for (int k = 0; k < num3; k++)
						{
							list.Add(this.binaryReader_0.ReadInt32());
						}
						item = new GClass3(list.ToArray());
						break;
					}
					case GEnum1.Type_FloatArr:
					{
						int num3 = (int)this.binaryReader_0.ReadInt16();
						System.Collections.Generic.List<float> list2 = new System.Collections.Generic.List<float>();
						for (int l = 0; l < num3; l++)
						{
							list2.Add(this.binaryReader_0.ReadSingle());
						}
						item = new GClass3(list2.ToArray());
						break;
					}
					default:
						item = new GClass3(0);
						break;
					}
					gClass.list_0.Add(item);
				}
				this.list_0.Add(gClass);
			}
		}
	}
	private void method_4(string string_2)
	{
		string[] array = string_2.Split(new char[]
		{
			','
		});
		for (int i = 0; i < array.Length; i++)
		{
			if (this.method_5(array[i]))
			{
				char[] array2 = new char[array[i].Length - 7];
				for (int j = 6; j < array[i].Length - 1; j++)
				{
					array2[j - 6] = array[i][j];
				}
				this.method_4(new string(array2));
			}
			else
			{
				try
				{
					int num = System.Convert.ToInt32(array[i]);
					this.list_1.Add(num);
				}
				catch
				{
					try
					{
						double num2 = System.Convert.ToDouble(array[i]);
						this.list_1.Add((float)num2);
					}
					catch
					{
						string item = array[i];
						this.list_1.Add(item);
					}
				}
			}
		}
	}
	private bool method_5(string string_2)
	{
		return string_2 != null && string_2.Length >= 7 && (string_2[0] == 'u' && string_2[1] == 'n' && string_2[2] == 'i' && string_2[3] == 't' && string_2[4] == 'e' && string_2[5] == '(') && string_2[string_2.Length - 1] == ')';
	}
	public GClass6 imethod_4()
	{
		return this.gclass6_0;
	}
	public GClass5[] imethod_3()
	{
		GClass5[] result;
		if (this.list_0.Count > 0)
		{
			result = this.list_0.ToArray();
		}
		else
		{
			result = null;
		}
		return result;
	}
}
