using System;
public class GClass3
{
	public object object_0;
	public object[] object_1;
	public GClass3(object object_2)
	{
		this.object_0 = object_2;
	}
	public GClass3(object[] object_2)
	{
		this.object_1 = new object[object_2.Length];
		for (int i = 0; i < object_2.Length; i++)
		{
			this.object_1[i] = object_2[i];
		}
	}
	public void method_0(object[] object_2)
	{
		this.object_1 = new object[object_2.Length];
		for (int i = 0; i < object_2.Length; i++)
		{
			this.object_1[i] = object_2[i];
		}
	}
	public int method_1()
	{
		return (int)this.object_0;
	}
	public float method_2()
	{
		return (float)this.object_0;
	}
	public string method_3()
	{
		return this.object_0.ToString();
	}
	public object[] method_4()
	{
		return this.object_1;
	}
}
