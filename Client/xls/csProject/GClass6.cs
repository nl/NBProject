using System;
using System.Collections.Generic;
public class GClass6
{
	private string string_0;
	private System.Collections.Generic.Dictionary<int, GClass4> dictionary_0 = new System.Collections.Generic.Dictionary<int, GClass4>();
	private System.Collections.Generic.Dictionary<string, GClass4> dictionary_1 = new System.Collections.Generic.Dictionary<string, GClass4>();
	public string method_0()
	{
		return this.string_0;
	}
	public void method_1(string string_1)
	{
		this.string_0 = string_1;
	}
	public int method_2()
	{
		return this.dictionary_0.Count;
	}
	public GClass4 method_3(GClass4 gclass4_0)
	{
		this.dictionary_0.Add(gclass4_0.int_0, gclass4_0);
		this.dictionary_1.Add(gclass4_0.string_6, gclass4_0);
		return gclass4_0;
	}
	public GClass4 method_4(string string_1)
	{
		GClass4 result = null;
		this.dictionary_1.TryGetValue(string_1, out result);
		return result;
	}
	public GClass4 method_5(int int_0)
	{
		GClass4 result = null;
		this.dictionary_0.TryGetValue(int_0, out result);
		return result;
	}
}
