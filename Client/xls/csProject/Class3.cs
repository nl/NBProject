using System;
internal class Class3 : GClass7<Class3>
{
	public GInterface0 method_0(string string_0)
	{
		GInterface0 result = null;
		string[] array = string_0.Split(new char[]
		{
			'/'
		});
		string text = array[array.Length - 1];
		string[] array2 = text.Split(new char[]
		{
			'.'
		});
		string text2 = array2[1];
		if (text2 != null)
		{
			if (!(text2 == "xls") && !(text2 == "xlsx"))
			{
				if (text2 == "bytes")
				{
					result = new GClass0(string_0);
				}
			}
			else
			{
				result = new GClass2(string_0);
			}
		}
		return result;
	}
	public string method_1(GEnum0 genum0_0)
	{
		string result;
		switch (genum0_0)
		{
		case GEnum0.const_0:
			result = "xls";
			break;
		case GEnum0.const_1:
			result = "bytes";
			break;
		default:
			result = "bytes";
			break;
		}
		return result;
	}
	public GInterface1 method_2(string string_0)
	{
		string[] array = string_0.Split(new char[]
		{
			'\\'
		});
		string text = array[array.Length - 1];
		string[] array2 = text.Split(new char[]
		{
			'.'
		});
		string text2 = array2[1];
		string text3 = text2;
		GInterface1 result;
		if (text3 != null && text3 == "bytes")
		{
			GInterface1 gInterface = new GClass1(string_0);
			GInterface1 gInterface2 = gInterface;
			result = gInterface2;
		}
		else
		{
			GInterface1 gInterface2 = null;
			result = gInterface2;
		}
		return result;
	}
}
