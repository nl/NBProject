using System;
using System.Collections.Generic;
using System.IO;
public class GClass11
{
	public static string smethod_0(string string_0)
	{
		string result;
		if (string.IsNullOrEmpty(string_0))
		{
			result = string_0;
		}
		else
		{
			if (string_0.Length > 1)
			{
				string_0 = string_0.Substring(0, 1).ToString().ToUpper() + string_0.Substring(1, string_0.Length - 1);
			}
			else
			{
				string_0 = string_0.ToUpper();
			}
			result = string_0;
		}
		return result;
	}
	public static string smethod_1(string string_0)
	{
		if (!System.IO.Directory.Exists(string_0))
		{
			System.IO.Directory.CreateDirectory(string_0);
		}
		return string_0;
	}
	public static string smethod_2(string string_0)
	{
		string_0 = string_0.Replace("\\", "/");
		string[] array = string_0.Split(new char[]
		{
			'/'
		});
		string text = array[array.Length - 1];
		string[] array2 = text.Split(new char[]
		{
			'.'
		});
		return array2[0];
	}
	public static string smethod_3(string string_0)
	{
		string_0 = string_0.Replace("\\", "/");
		string[] array = string_0.Split(new char[]
		{
			'/'
		});
		string text = array[array.Length - 1];
		string[] array2 = text.Split(new char[]
		{
			'.'
		});
		return array2[array2.Length - 1];
	}
	public static bool smethod_4(string string_0, string[] string_1)
	{
		bool result;
		if (string_1 == null || string_1.Length == 0)
		{
			result = false;
		}
		else
		{
			for (int i = 0; i < string_1.Length; i++)
			{
				if (!string.IsNullOrEmpty(string_1[i]) && string_1[i] == string_0)
				{
					result = true;
					return result;
				}
			}
			result = false;
		}
		return result;
	}
	public static string[] smethod_5(string string_0, string string_1, bool bool_0)
	{
		string[] string_2 = string_1.Split(new char[]
		{
			';'
		});
		System.Collections.Generic.List<string> list = new System.Collections.Generic.List<string>();
		System.Collections.Generic.List<string> list2 = new System.Collections.Generic.List<string>();
		list.Add(string_0);
		if (bool_0)
		{
			System.IO.DirectoryInfo directoryInfo = new System.IO.DirectoryInfo(string_0);
			System.IO.DirectoryInfo[] directories = directoryInfo.GetDirectories();
			for (int i = 0; i < directories.Length; i++)
			{
				System.IO.DirectoryInfo directoryInfo2 = directories[i];
				list.Add(directoryInfo2.FullName);
			}
		}
		foreach (string current in list)
		{
			System.IO.DirectoryInfo directoryInfo3 = new System.IO.DirectoryInfo(current);
			System.IO.FileInfo[] files = directoryInfo3.GetFiles();
			for (int i = 0; i < files.Length; i++)
			{
				System.IO.FileInfo fileInfo = files[i];
				string text = fileInfo.FullName;
				GClass11.smethod_2(text);
				string string_3 = GClass11.smethod_3(text);
				if (GClass11.smethod_4(string_3, string_2))
				{
					text = text.Replace("\\", "/");
					list2.Add(text);
				}
			}
		}
		return list2.ToArray();
	}
}
